# Does Every Data Instance Matter? Enhancing Sequential Recommendation by Eliminating Unreliable Data

## Requirements
- `Anaconda==4.8.5`
- `python==3.6`
- `pytorch==1.1.0`
- `pytorch_pretrained_bert==0.6.1`
- `CUDA==10.1`
- `cuDNN==7.5.1`


## Usage
1. Install required packages.
2. run <code>python ml1m-main.py</code> to train and evalluate BERD with ML-1M dataset. Similar scripts are prepared for Steam (<code>python ml1m-main.py</code>), Amazon-CDs (<code>python cd-main.py</code>) and Amazon-Electronics (<code>python elect-main.py</code>)

## Datasets
- All the datasets used in our paper are organized in [datasets/](datasets/), where each data file contains a list of triplets [userId itemId, rating] which are chronologically ranked for each user.
ML-1M is collected from https://grouplens.org/datasets/movielens/1m/,
Steam is from https://github.com/kang205/SASRec,
Amazon-CDs and Amazon-Electronics are from http://jmcauley.ucsd.edu/data/amazon/.

- Note that the user-item graphs are pre-constructed as s_pre_adj_mat_1.npz for all datasets except Steam due to space limitation. Thus the first run on Steam dataset might be a little time-consuming when constructing the graph.

## Results
The detailed training logs for each dataset are availble in [log/](log/).

## Codes for other baselines
GRU4Rec: https://github.com/hidasib/GRU4Rec
Caser: https://github.com/graytowne/caser_pytorch
SASRec: https://github.com/kang205/SASRec
BERT4Rec: https://github.com/FeiSun/BERT4Rec
GC-SAN: https://github.com/johnny12150/GC-SAN
HGN: https://github.com/allenjack/HGN
