import torch
from torch import nn
import math


class GraphContextEncoder(nn.Module):
    def __init__(self, config):
        super().__init__()
        self.user_embeddings = nn.Embedding(config['user_num'], config['hidden_size'])
        self.item_embeddings = nn.Embedding(config['item_num'], config['hidden_size'], padding_idx=0)

    def forward(self, user_id, hist_item_ids, masks, user_prop_embeds, item_prop_embeds):
        raise NotImplementedError


class RobGraphContextEncoder(GraphContextEncoder):
    def __init__(self, config):
        super().__init__(config)
        self.user_var = nn.Embedding(config['user_num'], 1)
        self.item_var = nn.Embedding(config['item_num'], 1, padding_idx=0)


class GraphSeqRec(nn.Module):
    def __init__(self, config, context_encoder: GraphContextEncoder, Graph):
        super().__init__()
        self.config = config
        self.Graph = Graph
        if self.config['train_type'] == 'train':
            self.context_encoder = context_encoder
            self.apply(self._init_parameters)
        # elif self.config['train_type'] == 'eval':
        #     self.context_encoder = self.load_model()
        self.merge_weights = self.build_merge_weights()

    def set_encoder(self, encoder):
        self.context_encoder = encoder

    def _init_parameters(self, module):
        """ Initialize the parameterss.
        """
        if isinstance(module, nn.Embedding):
            hidden_size = module.weight.size()[1]
            bound = 6 / math.sqrt(hidden_size)
            nn.init.uniform_(module.weight, a=-bound, b=bound)
        if isinstance(module, nn.Linear):
            torch.nn.init.xavier_normal_(module.weight, gain=1.0)
        if isinstance(module, nn.Linear) and module.bias is not None:
            module.bias.data.zero_()

    def build_merge_weights(self):
        merge_weights = []
        for i in range(self.config['n_layers']):
            merge_weights.append(1 / (i + 1))
        merge_weights = torch.tensor(merge_weights)
        if torch.cuda.is_available():
            merge_weights = merge_weights.to(torch.device('cuda')).double()
        merge_weights = merge_weights.unsqueeze(0).unsqueeze(0)
        # [1, 1, num_layer]
        return merge_weights

    def forward(self, train_batch):
        user_id, hist_item_ids, masks, pos_target, neg_targets, sample_idices = train_batch
        if torch.cuda.is_available():
            user_id = user_id.to(torch.device('cuda'))
            hist_item_ids = hist_item_ids.to(torch.device('cuda'))
            masks = masks.to(torch.device('cuda'))
            pos_target = pos_target.to(torch.device('cuda'))
            neg_targets = neg_targets.to(torch.device('cuda'))
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        pos_target = [bs]
        neg_targets = [bs, neg_num]
        """
        user_prop_embeds, item_prop_embeds = self.propagate_embeds(is_training=True)
        neg_num = neg_targets.size()[1]
        # [bs, hidden_size]
        context_embed = self.context_encoder(user_id, hist_item_ids, masks, user_prop_embeds, item_prop_embeds)
        # [bs, 1]
        pos_score = torch.mul(context_embed, item_prop_embeds(pos_target)).sum(dim=1, keepdim=True)
        # [bs, neg_num, hidden_size]
        neg_embeds = item_prop_embeds(neg_targets)
        # [bs, neg_num]
        neg_scores = torch.mul(context_embed.unsqueeze(1), neg_embeds).sum(dim=2, keepdim=False)
        # [bs], BPR loss
        overall_loss = -(torch.log(torch.sigmoid(pos_score - neg_scores)) / neg_num).sum(dim=1, keepdim=False)

        return overall_loss, sample_idices

    def eval_ranking(self, test_batch):
        user_id, hist_item_ids, masks, target_ids = test_batch
        masks = masks.float()
        if torch.cuda.is_available():
            user_id = user_id.to(torch.device('cuda'))
            hist_item_ids = hist_item_ids.to(torch.device('cuda'))
            masks = masks.double().to(torch.device('cuda'))
            target_ids = target_ids.to(torch.device('cuda'))
            # print(f'eval_ranking: {user_id.is_cuda}')
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        target_ids = [bs, eval_neg_num + 2]
        """
        ranks = self._get_test_scores(user_id, hist_item_ids, masks, target_ids).argsort(
            dim=1, descending=True).argsort(dim=1, descending=False)[:, 0:1].float()

        # evaluate ranking
        metrics = {
            'ndcg_1': 0,
            'ndcg_5': 0,
            'ndcg_10': 0,
            'ndcg_20': 0,
            'hit_1': 0,
            'hit_5': 0,
            'hit_10': 0,
            'hit_20': 0,
            'ap': 0,
        }
        for rank in ranks:
            if rank < 1:
                metrics['ndcg_1'] += 1
                metrics['hit_1'] += 1
            if rank < 5:
                metrics['ndcg_5'] += 1 / torch.log2(rank + 2)
                metrics['hit_5'] += 1
            if rank < 10:
                metrics['ndcg_10'] += 1 / torch.log2(rank + 2)
                metrics['hit_10'] += 1
            if rank < 20:
                metrics['ndcg_20'] += 1 / torch.log2(rank + 2)
                metrics['hit_20'] += 1
            metrics['ap'] += 1.0 / (rank + 1)
        return metrics

    def _get_test_scores(self, user_id, hist_item_ids, masks, target_ids):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        target_ids = [bs, 1 + eval_neg_num]
        """
        user_prop_embeds, item_prop_embeds = self.propagate_embeds(is_training=False)
        # [bs, 1, hidden_size]
        context_embed = self.context_encoder(user_id, hist_item_ids, masks, user_prop_embeds, item_prop_embeds).unsqueeze(1)
        # [bs, pred_num, hidden_size]
        target_embeds = item_prop_embeds.item_embeddings(target_ids)
        # [bs, pred_num]
        scores = torch.mul(context_embed, target_embeds).sum(dim=2, keepdim=False)

        return scores

    def propagate_embeds(self, is_training):
        users_emb = self.context_encoder.user_embeddings.weight
        items_emb = self.context_encoder.item_embeddings.weight
        all_emb = torch.cat([users_emb, items_emb])
        #   torch.split(all_emb , [self.num_users, self.num_items])
        embs = [all_emb]
        for layer in range(self.config['n_layers']):
            all_emb = torch.sparse.mm(self.Graph, all_emb)
            embs.append(all_emb)
        # [numUser + numItem, hidden_size, n_layers]
        embs = torch.stack(embs, dim=2) * self.merge_weights
        # print(embs.size())
        # [numUser + numItem, 1, n_layers]
        light_out = torch.mean(embs, dim=2)
        users, items = torch.split(light_out, [self.num_users, self.num_items])
        return users, items

    def __dropout_x(self, x, keep_prob):
        size = x.size()
        index = x.indices().t()
        values = x.values()
        random_index = torch.rand(len(values)) + keep_prob
        random_index = random_index.int().bool()
        index = index[random_index]
        values = values[random_index] / keep_prob
        g = torch.sparse.FloatTensor(index.t(), values, size)
        return g

    def __dropout(self, keep_prob):
        if self.A_split:
            graph = []
            for g in self.Graph:
                graph.append(self.__dropout_x(g, keep_prob))
        else:
            graph = self.__dropout_x(self.Graph, keep_prob)
        return graph


class ProbGraphSeqRec(GraphSeqRec):

    def __init__(self, config, context_encoder: GraphContextEncoder, Graph):
        super().__init__(config, context_encoder, Graph)

    def forward(self, train_batch):
        user_id, hist_item_ids, masks, pos_target, neg_targets, sample_idices = train_batch
        if torch.cuda.is_available():
            user_id = user_id.to(torch.device('cuda'))
            hist_item_ids = hist_item_ids.to(torch.device('cuda'))
            masks = masks.to(torch.device('cuda'))
            pos_target = pos_target.to(torch.device('cuda'))
            neg_targets = neg_targets.to(torch.device('cuda'))
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        pos_target = [bs]
        neg_targets = [bs, neg_num]
        """
        pos_target = pos_target.unsqueeze(1)
        user_prop_embeds, item_prop_embeds = self.propagate_embeds(is_training=True)
        neg_num = neg_targets.size()[1]
        # [bs, hidden_size]
        context_mean, context_var = self.context_encoder(user_id, hist_item_ids, masks, user_prop_embeds, item_prop_embeds)
        # [bs, 1. hidden_size]
        unseq_context_mean = context_mean.unsqueeze(1)
        unseq_context_var = context_var.unsqueeze(1)

        # [bs, 1], [bs, sample_num, 1]
        pos_W_score, pos_sample_scores = self.prediction_score(unseq_context_mean, unseq_context_var, pos_target, item_prop_embeds)
        # [bs, neg_num], [bs, sample_num, neg_num]
        neg_W_scores, neg_sample_scores = self.prediction_score(unseq_context_mean, unseq_context_var, neg_targets, item_prop_embeds)

        # [bs], BPR loss
        # [bs, 1] - [bs, neg_num] = [bs, neg_num] -sum-> [bs]
        rec_W_loss = -torch.log(torch.sigmoid(pos_W_score - neg_W_scores) + 1e-8).mean(dim=1, keepdim=False)
        # [bs, sample_num, 1] - [bs, sample_num, neg_num]
        # = [bs, sample_num, neg_num] -mean-> [bs, sample_num] -mean-> [bs]
        rec_sample_loss = -torch.log(torch.sigmoid(pos_sample_scores - neg_sample_scores) + 1e-8).mean(dim=2, keepdim=False).mean(dim=1, keepdim=False)
        # entropy reg
        # [bs]
        var_entropy = torch.log(context_var + 1e-8).sum(dim=1)
        entropy_reg = torch.relu(self.config['entropy_threshold'] - var_entropy)
        # [bs]
        rec_loss = rec_W_loss + self.config['sample_loss_weight'] * rec_sample_loss

        return rec_loss + self.config['entropy_reg_weight'] * entropy_reg, rec_loss, var_entropy, sample_idices

    def _get_test_scores(self, user_id, hist_item_ids, masks, target_ids):
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        target_ids = [bs, 1 + eval_neg_num]
        """
        user_prop_embeds, item_prop_embeds = self.propagate_embeds(is_training=False)
        # [bs, hidden_size]
        context_mean, context_var = self.context_encoder(user_id, hist_item_ids, masks, user_prop_embeds, item_prop_embeds)
        # [bs, 1, hidden_size]
        unseq_context_mean = context_mean.unsqueeze(1)
        unseq_context_var = torch.relu(context_var.unsqueeze(1))
        # [bs, pred_num], [bs, sample_num, pred_num]
        W_scores, sample_scores = self.prediction_score(unseq_context_mean, unseq_context_var, target_ids, item_prop_embeds)

        return W_scores + self.config['sample_loss_weight'] * sample_scores.mean(dim=1, keepdim=False)

    def prediction_score(self, context_mean, context_var, target_item_ids, prop_item_embeds):
        """
        context_mean = [bs, 1, hidden_size]
        context_var = [bs, 1, hidden_size]
        target_item_ids = [bs, pred_num]
        prop_item_embeds = [itemNum, hidden_size]
        """
        # [bs, pred_num, hidden_size]
        target_item_embed = prop_item_embeds[target_item_ids]
        # [bs, pred_num]
        W_scores = torch.mul(context_mean, target_item_embed).sum(dim=2, keepdim=False)

        # sample scores: [bs, sample_num, 1]
        random_normal_values = torch.randn(context_var.shape[0], self.config['sample_num'], 1)
        if torch.cuda.is_available():
            random_normal_values = random_normal_values.to(torch.device('cuda')).double()
        # [bs, sample_num, hidden_size]
        context_embed = context_mean + torch.mul(context_var, random_normal_values)
        # [bs, sample_num, 1, hidden_size] * [bs, 1, pred_num, hidden_size] =
        # [bs, sample_num, pred_num, hidden_size] -sum-> [bs, sample_num, pred_num]
        sample_scores = torch.mul(context_embed.unsqueeze(2), target_item_embed.unsqueeze(1)).sum(dim=3)
        # [bs, pred_num], [bs, sample_num, pred_num]
        return W_scores, sample_scores


class RobGraphSeqRec(ProbGraphSeqRec):

    def __init__(self, config, context_encoder: RobGraphContextEncoder, Graph):
        super().__init__(config, context_encoder, Graph)
        self.config['n_layers'] = 1

    def forward(self, train_batch):
        user_id, hist_item_ids, masks, pos_target, neg_targets, sample_idices = train_batch
        if torch.cuda.is_available():
            user_id = user_id.to(torch.device('cuda'))
            hist_item_ids = hist_item_ids.to(torch.device('cuda'))
            masks = masks.to(torch.device('cuda'))
            pos_target = pos_target.to(torch.device('cuda'))
            neg_targets = neg_targets.to(torch.device('cuda'))
        """
        user_id = [bs]
        hist_item_ids = [bs, seq_len]
        masks = [bs, seq_len]
        pos_target = [bs]
        neg_targets = [bs, neg_num]
        """
        pos_target = pos_target.unsqueeze(1)
        user_prop_embeds, item_prop_embeds = self.propagate_embeds_light(is_training=True)
        # neg_num = neg_targets.size()[1]
        # [bs, hidden_size]
        context_mean, context_var = self.context_encoder(user_id, hist_item_ids, masks, user_prop_embeds, item_prop_embeds)
        # [bs, 1. hidden_size]
        unseq_context_mean = context_mean.unsqueeze(1)
        unseq_context_var = context_var.unsqueeze(1)

        # [bs, 1], [bs, sample_num, 1]
        pos_W_score, pos_sample_scores = self.prediction_score(unseq_context_mean, unseq_context_var, pos_target, item_prop_embeds)
        # [bs, neg_num], [bs, sample_num, neg_num]
        neg_W_scores, neg_sample_scores = self.prediction_score(unseq_context_mean, unseq_context_var, neg_targets, item_prop_embeds)

        # [bs], BPR loss
        # [bs, 1] - [bs, neg_num] = [bs, neg_num] -sum-> [bs]
        rec_W_loss = -torch.log(torch.sigmoid(pos_W_score - neg_W_scores) + 1e-8).mean(dim=1, keepdim=False)
        # [bs, sample_num, 1] - [bs, sample_num, neg_num]
        # = [bs, sample_num, neg_num] -mean-> [bs, sample_num] -mean-> [bs]
        rec_sample_loss = -torch.log(torch.sigmoid(pos_sample_scores - neg_sample_scores) + 1e-8).mean(dim=2, keepdim=False).mean(dim=1, keepdim=False)
        # entropy reg
        # [bs]
        var_entropy = torch.log(torch.sqrt(context_var) + 1e-8).sum(dim=1)
        entropy_reg = torch.relu(self.config['entropy_threshold'] - var_entropy) * 1e-2
        # [bs]
        rec_loss = rec_W_loss + self.config['sample_loss_weight'] * rec_sample_loss

        return rec_loss + entropy_reg, rec_loss, var_entropy, sample_idices

    def propagate_embeds(self, is_training):
        users_emb = self.context_encoder.user_embeddings.weight
        items_emb = self.context_encoder.item_embeddings.weight
        users_var = self.context_encoder.user_var.weight
        items_var = self.context_encoder.item_var.weight
        all_emb = torch.cat([users_emb, items_emb])
        # [numUser + numItem, 1]
        all_var = torch.cat([users_var, items_var])
        # [numUser + numItem, 1]
        comp_weight = torch.sigmoid(all_var)
        # [numUser + numItem, 1]
        self_weight = 1 - comp_weight
        # [[numUser + numItem, hidden_size]]
        embs = [all_emb]
        local_graph = self.Graph
        for layer in range(self.config['n_layers']):
            # prop control
            transed_weight = torch.transpose(self_weight, 0, 1)
            expanded_weight = transed_weight.expand(local_graph.size()[0], local_graph.size()[0])
            local_graph = sparse_dense_mul(local_graph, expanded_weight)
            all_emb = torch.sparse.mm(local_graph, all_emb)
            embs.append(all_emb)
        # [numUser + numItem, hidden_size, n_layers + 1]
        embs = torch.stack(embs, dim=2)
        # [numUser + numItem, n_layers]
        expanded_comp_weights = comp_weight.expand([self_weight.size()[0], self.config['n_layers']])
        # [numUser + numItem, 1, n_layers + 1]
        merge_weights = torch.cat([self_weight, expanded_comp_weights], dim=1).unsqueeze(1)
        embs = embs * merge_weights
        embs = embs * self.merge_weights
        # [numUser + numItem, hidden_size]
        light_out = torch.mean(embs, dim=2)
        users, items = torch.split(light_out, [self.config['user_num'], self.config['item_num']])
        return users, items

    def propagate_embeds_light(self, is_training):
        users_emb = self.context_encoder.user_embeddings.weight
        items_emb = self.context_encoder.item_embeddings.weight
        users_var = self.context_encoder.user_var.weight
        items_var = self.context_encoder.item_var.weight
        all_emb = torch.cat([users_emb, items_emb])
        # [numUser + numItem, 1]
        all_var = torch.cat([users_var, items_var])
        # [[numUser + numItem, hidden_size]]
        embs = [all_emb]
        g_droped = self.Graph
        for layer in range(self.config['n_layers']):
            all_emb = torch.sparse.mm(g_droped, all_emb)
            embs.append(all_emb)
        # [numUser + numItem, hidden_size, n_layers + 1]
        embs = torch.stack(embs, dim=2)
        # [numUser + numItem, 1]
        comp_weight = torch.sigmoid(all_var)
        # [numUser + numItem, 1]
        self_weight = 1 - comp_weight
        # [numUser + numItem, n_layers]
        expanded_comp_weights = comp_weight.expand([self_weight.size()[0], self.config['n_layers']])
        # [numUser + numItem, 1, n_layers + 1]
        merge_weights = torch.cat([self_weight, expanded_comp_weights], dim=1).unsqueeze(1)
        embs = embs * merge_weights
        embs = embs * self.merge_weights
        # [numUser + numItem, hidden_size]
        light_out = torch.mean(embs, dim=2)
        users, items = torch.split(light_out, [self.config['user_num'], self.config['item_num']])
        return users, items

def sparse_dense_mul(s, d):
    i = s._indices()
    v = s._values()
    dv = d[i[0, :], i[1, :]]  # get values from relevant entries of dense matrix
    return torch.sparse.FloatTensor(i, v * dv, s.size())