from data_utils.RankingEvaluator import RankingEvaluator
import torch
from torch import optim
import time
import os
import numpy as np
from torch.optim.lr_scheduler import ReduceLROnPlateau
from model.GraphSeq import RobGraphSeqRec
from model.BERD import BERD


class Trainer:
    def __init__(self, config, data_model, save_dir):

        train_loader = data_model.generate_train_dataloader_unidirect()
        test_loader = data_model.generate_test_dataloader_unidirect()

        self.config = config
        self.save_dir = save_dir
        self.train_type = config['train_type']
        self.rec_model = config['rec_model']

        self.train_loader = train_loader
        self._evaluator_1 = RankingEvaluator(test_loader)
        self._evaluator_2 = RankingEvaluator(test_loader)
        self.item_dist = np.array(data_model.item_dist)
        self.user_dist = np.array(data_model.user_dist)
        self.train_size = len(train_loader.dataset)
        self.model_save_dir = './datasets/' + self.config['dataset'] + '/model/'
        self.model_save_path = self.model_save_dir + self.rec_model + str(self.config['sample_loss_weight']) + '-'
        self.save_epochs = self.config['save_epochs']

        graph = data_model.getSparseGraph()
        rec_model_1 = RobGraphSeqRec(config, BERD(config), graph)
        rec_model_2 = RobGraphSeqRec(config, BERD(config), graph)

        if self.train_type == 'train':
            if rec_model_1 is not None:
                self._model_1 = rec_model_1
                self._device = config['device']
                self._model_1.double().to(self._device)
                self._optimizer_1 = _get_optimizer(
                    self._model_1, learning_rate=config['learning_rate'], weight_decay=config['weight_decay'])
                self.scheduler_1 = ReduceLROnPlateau(self._optimizer_1, 'max', patience=10,
                                                     factor=config['decay_factor'])
            if rec_model_2 is not None:
                self._model_2 = rec_model_2
                self._model_2.double().to(self._device)
                self._optimizer_2 = _get_optimizer(
                    self._model_2, learning_rate=config['learning_rate'], weight_decay=config['weight_decay'])
                self.scheduler_2 = ReduceLROnPlateau(self._optimizer_2, 'max', patience=10,
                                                     factor=config['decay_factor'])
            else:
                self._model_2 = None
            self.forget_rates = self.build_forget_rates()

        elif self.train_type == 'eval':
            self._device = config['device']
            self._model_1 = rec_model_1

    def build_forget_rates(self):
        forget_rates = np.ones(self.config['epoch_num']) * 0.06
        forget_rates[:20] = np.linspace(0, 0.06, 20)
        return forget_rates

    def save_model(self, epoch_num):
        if not os.path.exists(self.model_save_dir):
            os.makedirs(self.model_save_dir)
        save_path = self.model_save_path + str(epoch_num) + '-model.pkl'
        torch.save(self._model_1.context_encoder, save_path)
        print(f'model saved at {save_path}')

    def load_model(self, epoch_num):
        load_path = self.model_save_path + str(epoch_num) + '-model.pkl'
        print(f'loading model from {load_path}')
        return torch.load(load_path)

    def co_train_one_batch(self, batch, epoch_num, batch_num):
        self._model_1.train()
        self._optimizer_1.zero_grad()
        self._model_2.train()
        self._optimizer_2.zero_grad()
        # [bs], [bs], [bs]
        overall_loss_1, rec_loss_1, entropy_1, sample_idices = self._model_1(batch)
        overall_loss_2, rec_loss_2, entropy_2, _ = self._model_2(batch)

        loss_1_update, loss_2_update = self.co_teaching(
            overall_loss_1, rec_loss_1, entropy_1, overall_loss_2, rec_loss_2, entropy_2, epoch_num)

        loss_1_update.backward()
        self._optimizer_1.step()
        loss_2_update.backward()
        self._optimizer_2.step()

        return loss_1_update, loss_2_update

    def co_teaching(self, overall_loss_1, rec_loss_1, entropy_1, overall_loss_2, rec_loss_2, entropy_2, epoch_num):
        list_len = len(rec_loss_1)
        num_forget = int(self.forget_rates[epoch_num] * list_len)
        loss_1_kept_bool, ind_1_removed = self.select_valid_overall_loss(rec_loss_1, entropy_1, list_len, num_forget)
        loss_2_kept_bool, ind_2_removed = self.select_valid_overall_loss(rec_loss_2, entropy_2, list_len, num_forget)
        loss_1_update = overall_loss_1 * loss_2_kept_bool
        loss_2_update = overall_loss_2 * loss_1_kept_bool

        return loss_1_update.sum(), loss_2_update.sum()

    def select_valid_overall_loss(self, loss, entropy, list_len, num_forget): # remove those
        ind_loss_sorted = np.argsort(loss.cpu().data)  # loss: the higher the better
        ind_entropy_sorted = np.argsort(entropy.cpu().data)  # entropy: the lower the better
        remo_entropy_ind = set(ind_entropy_sorted[0: int(num_forget * 5)].tolist())
        if num_forget is 0:
            remo_loss_ind = set()
        else:
            remo_loss_ind = set(ind_loss_sorted[-num_forget:].tolist())
        remo_overall_loss_ind = list(remo_loss_ind.intersection(remo_entropy_ind))
        loss_kept_bool = np.ones(list_len)
        loss_kept_bool[remo_overall_loss_ind] = 0

        return torch.tensor(loss_kept_bool).to(self._device), remo_overall_loss_ind

    def run_co(self):
        if self.train_type == 'train':
            print('=' * 60, '\n', 'Start Training', '\n', '=' * 60, sep='')
            keep_train = True
            for epoch in range(self.config['epoch_num']):
                start = time.time()
                loss_1_iter = 0
                loss_2_iter = 0
                for i, batch in enumerate(self.train_loader):
                    loss_1, loss_2 = self.co_train_one_batch(batch, epoch, i)
                    loss_1_iter += loss_1.item()
                    loss_2_iter += loss_2.item()
                print(f'################## epoch {epoch} ###########################')
                print(f"loss: {round(loss_1_iter / len(self.train_loader), 4)}, len_train_loader:{len(self.train_loader)}")
                keep_train = self.evaluate(epoch)
                print('#########################################################')
                if epoch in self.save_epochs:
                    self.save_model(epoch)
                if not keep_train:
                    break
        elif self.train_type == 'eval':
            for epoch in self.save_epochs:
                self._model_1.set_encoder(self.load_model(epoch))
                self._model_1.double().to(self._device)
                self._evaluator_1.evaluate(model=self._model_1, train_iter=0)

    def evaluate(self, iter):
        self._model_1.eval()
        keep_train_1, ndcg10_1 = self._evaluator_1.evaluate(model=self._model_1, train_iter=iter)
        self.scheduler_1.step(ndcg10_1)
        keep_train_2 = False
        if self._model_2 is not None:
            self._model_2.eval()
            keep_train_2, ndcg10_2 = self._evaluator_2.evaluate(model=self._model_2, train_iter=iter, verbose=False)
            self.scheduler_2.step(ndcg10_2)
        return any((keep_train_1, keep_train_2))

    @property
    def model(self):
        return self._model_1

    @property
    def optimizer(self):
        return self._optimizer_1


def _get_optimizer(model, learning_rate, weight_decay=0.01):
    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if
                    not any(nd in n for nd in no_decay)], 'weight_decay': weight_decay},
        {'params': [p for n, p in param_optimizer if
                    any(nd in n for nd in no_decay)], 'weight_decay': 0.0}]

    return optim.Adam(optimizer_grouped_parameters, lr=learning_rate)


def set2str(input_set):
    set_str = ''
    set_len = len(input_set)
    for i, item in enumerate(input_set):
        if i < set_len - 1:
            set_str += str(item) + ','
        else:
            set_str += str(item)
    return set_str

